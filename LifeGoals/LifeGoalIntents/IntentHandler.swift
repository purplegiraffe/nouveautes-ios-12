//
//  IntentHandler.swift
//  LifeGoalIntents
//
//  Created by Maxime Britto on 12/07/2018.
//  Copyright © 2018 Purple Giraffe. All rights reserved.
//

import Intents

class IntentHandler: INExtension, AchieveGoalIntentHandling {
    func handle(intent: AchieveGoalIntent, completion: @escaping (AchieveGoalIntentResponse) -> Void) {
        let goalManager = GoalManager()
        if let goalName = intent.goalName,
            let goal = goalManager.getGoal(withTitle: goalName) {
            let achievementCount = Int(truncating: intent.achievementCount ?? 1)
            for _ in 0..<achievementCount {
                goal.addAchievement()
            }
        }
    }
    
    
    override func handler(for intent: INIntent) -> Any {
        // This is the default implementation.  If you want different objects to handle different intents,
        // you can override this and return the handler you want for that particular intent.
        
        return self
    }
    
}
