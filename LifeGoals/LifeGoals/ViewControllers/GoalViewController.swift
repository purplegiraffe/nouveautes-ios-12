//
//  GoalViewController.swift
//  LifeGoals
//
//  Created by Maxime Britto on 10/07/2018.
//  Copyright © 2018 Purple Giraffe. All rights reserved.
//

import UIKit
import Intents
import UserNotifications

extension NSUserActivity {
    static let displayGoalActivityType = "fr.purplegiraffe.lifegoal.activity.type.displayGoal"
}

class GoalViewController: UIViewController {
    var goal:Goal?
    
    @IBOutlet weak var ui_titleLabel: UILabel!
    @IBOutlet weak var ui_achievementCountLabel: UILabel!
    
    @IBOutlet weak var ui_frequencySelector: UISegmentedControl!
    
    @IBAction func goalFrequencyRateChanged() {
        guard let goal = self.goal else { return }
        switch ui_frequencySelector.selectedSegmentIndex {
        case 0:
            goal.setFrequency(rate: .none)
        case 1:
            goal.setFrequency(rate: .daily)
        case 2:
            goal.setFrequency(rate: .weekly)
        default:
            goal.setFrequency(rate: .none)
        }
    }
    
    
    @IBAction func backButtonTouched() {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func addAchievementButtonTouched() {
        if let goal = self.goal {
            goal.addAchievement()
            updateAchievementsCountLabel(withGoal: goal)
            if #available(iOS 12.0, *) {
                donateUserAchievementIntent(withGoal: goal)
            }
            if goal.repeatDayFrequency > 0 {
                //contenu
                let content = UNMutableNotificationContent()
                content.title = goal.title
                if goal.repeatDayFrequency == 1 {
                    content.threadIdentifier = "daily-tasks"
                }
                content.categoryIdentifier = "goal"
                
                //trigger
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(goal.repeatDayFrequency * 12), repeats: false)
                
                //requete
                let request = UNNotificationRequest(identifier: goal.title, content: content, trigger: trigger)
                
                //donne la requete
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
        }
    }
    
    @available(iOS 12.0, *)
    private func donateUserAchievementIntent(withGoal goal:Goal) {
        let intent = AchieveGoalIntent()
        intent.goalName = goal.title
        intent.achievementCount = NSNumber(value: 1)
        let interaction = INInteraction(intent: intent, response: nil)
        interaction.donate(completion: nil)
    }
    
    private func updateAchievementsCountLabel(withGoal goal:Goal) {
        ui_achievementCountLabel.text = "\(goal.achievements.count) fois"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let goal = self.goal {
            ui_titleLabel.text = goal.title
            updateAchievementsCountLabel(withGoal: goal)
            donateUserActivity(forGoal: goal)
            if let frequency = goal.getFrequency() {
                switch frequency {
                case .none:
                    ui_frequencySelector.selectedSegmentIndex = 0
                case .daily:
                    ui_frequencySelector.selectedSegmentIndex = 1
                case .weekly:
                    ui_frequencySelector.selectedSegmentIndex = 2
                }
            }
        }
    }
    
    private func donateUserActivity(forGoal goal:Goal) {
        let activity = NSUserActivity(activityType:NSUserActivity.displayGoalActivityType)
        activity.title = goal.title
        activity.isEligibleForSearch = true
        if #available(iOS 12.0, *) {
            activity.isEligibleForPrediction = true
            //activity.suggestedInvocationPhrase = "Coucou"
        }
        self.userActivity = activity
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
