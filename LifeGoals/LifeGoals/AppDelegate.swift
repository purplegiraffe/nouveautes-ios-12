//
//  AppDelegate.swift
//  LifeGoals
//
//  Created by Maxime Britto on 31/07/2017.
//  Copyright © 2017 Purple Giraffe. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        prepareNotifications()
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivity.displayGoalActivityType,
            let goalTitle = userActivity.title,
            let goal = GoalManager().getGoal(withTitle: goalTitle),
            var sourceVC = application.keyWindow?.rootViewController,
            let destinationVC = sourceVC.storyboard?.instantiateViewController(withIdentifier: "GoalViewController") as? GoalViewController {
            destinationVC.goal = goal
            
            while sourceVC.presentedViewController != nil {
                sourceVC = sourceVC.presentedViewController!
            }
            sourceVC.present(destinationVC, animated: true, completion: nil)
            return true
        }
        
        return false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate : UNUserNotificationCenterDelegate {
    private func prepareNotifications() {
        let notifCenter = UNUserNotificationCenter.current()
        
        if #available(iOS 12.0, *) {
            notifCenter.requestAuthorization(options: [.alert,.badge,.sound,.provisional]) { (_, _) in
                
            }
        } else {
            notifCenter.requestAuthorization(options: [.alert,.badge,.sound]) { (_, _) in
                
            }
        }
        notifCenter.delegate = self
        let goalCat:UNNotificationCategory
        if #available(iOS 12.0, *) {
            goalCat = UNNotificationCategory(identifier: "goal", actions: [], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "%u objectif(s)", categorySummaryFormat: "%u autre objectif(s)", options: [])
        } else {
            goalCat = UNNotificationCategory(identifier: "goal", actions: [], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "%u objectif(s)", options: [])
        }
        notifCenter.setNotificationCategories([goalCat])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound,.badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        
    }
    
    
}

