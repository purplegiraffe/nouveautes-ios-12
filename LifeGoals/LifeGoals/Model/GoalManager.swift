//
//  GoalManager.swift
//  LifeGoals
//
//  Created by Maxime Britto on 31/07/2017.
//  Copyright © 2017 Purple Giraffe. All rights reserved.
//

import Foundation
import RealmSwift
class GoalManager {
    private let GOAL_LIST_KEY = "GoalList"
    private var _goalList : Results<Goal>
    private var _realm:Realm
    
    init(realmConfig:Realm.Configuration? = nil) {
        if let config = realmConfig {
            _realm = try! Realm(configuration: config)
        } else {
            if let realmSharedFileUrl = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.fr.purplegiraffe.LifeGoals")?.appendingPathComponent("sharedDatabase.realm") {
                var config = Realm.Configuration.defaultConfiguration
                config.fileURL = realmSharedFileUrl
                config.deleteRealmIfMigrationNeeded = true
                Realm.Configuration.defaultConfiguration = config
            }
            _realm = try! Realm()
        }
        _goalList = _realm.objects(Goal.self)
        migrateFromUserDefaults()
        
    }
    
    private func migrateFromUserDefaults() {
        if let loadedGoalList = UserDefaults.standard.array(forKey: GOAL_LIST_KEY) as? [String] {
            for goalText in loadedGoalList {
                _ = addGoal(withText: goalText)
            }
            UserDefaults.standard.set(nil, forKey: GOAL_LIST_KEY)
        }
    }
    
    func getGoalCount() -> Int {
        return _goalList.count
    }
    
    func getGoal(atIndex index:Int) -> Goal {
        return _goalList[index]
    }
    
    func getGoal(withTitle titleToFind:String) -> Goal? {
        return _goalList.filter("title == %@", titleToFind).first
    }
    
    func addGoal(withText text:String) -> Int? {
        let newIndex:Int?
        if text.count > 0 {
            let newGoal = Goal(newTitle: text)
            try! _realm.write {
                _realm.add(newGoal)
            }
            newIndex = _goalList.index(of: newGoal)
        } else {
            newIndex = nil
        }
        return newIndex
    }
    
    func removeGoal(atIndex index:Int) {
        try! _realm.write {
            _realm.delete(getGoal(atIndex: index))
        }
    }
}
