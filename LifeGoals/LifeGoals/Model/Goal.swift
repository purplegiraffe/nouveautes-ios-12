//
//  Goal.swift
//  LifeGoals
//
//  Created by Maxime Britto on 01/08/2017.
//  Copyright © 2017 Purple Giraffe. All rights reserved.
//

import Foundation
import RealmSwift

class Goal : Object {
    enum RepetitionRate : Int {
        case none = 0
        case daily = 1
        case weekly = 7
    }
    @objc dynamic var title:String = ""
    @objc dynamic var repeatDayFrequency = 0
    let achievements = LinkingObjects(fromType: Achievement.self, property: "goal")
    
    var isDone: Bool {
        let isDone : Bool
        if achievements.count == 0 {
            isDone = false
        } else {
            if repeatDayFrequency == 0 {
                isDone = true
            } else {
                let lastAchievement = achievements.last!
                let now = Date()
                let calendar = NSCalendar.current
                let dayCount = calendar.dateComponents([.day], from: lastAchievement.date, to: now).day!
                isDone = (dayCount <= repeatDayFrequency)
            }
        }
        return isDone
    }
    
    convenience init(newTitle:String) {
        self.init()
        title = newTitle
    }
    
    func addAchievement() {
        guard let realm = self.realm else { return }
        let achievement = Achievement(goal: self)
        try? realm.write {
            realm.add(achievement)
        }
    }
    
    func setFrequency(rate: RepetitionRate) {
        guard let realm = self.realm else { return }
        try? realm.write {
            self.repeatDayFrequency = rate.rawValue
        }
    }
    
    func getFrequency() -> RepetitionRate? {
        return RepetitionRate(rawValue: self.repeatDayFrequency)
    }
    
}
