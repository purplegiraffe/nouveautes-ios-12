//
//  Achievement.swift
//  LifeGoals
//
//  Created by Maxime Britto on 10/07/2018.
//  Copyright © 2018 Purple Giraffe. All rights reserved.
//

import RealmSwift

class Achievement: Object {
    @objc dynamic var goal:Goal?
    @objc dynamic var date:Date = Date()
    
    convenience init(goal:Goal) {
        self.init()
        self.goal = goal
    }
}
