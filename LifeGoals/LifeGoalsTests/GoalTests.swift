//
//  LifeGoalsTests.swift
//  LifeGoalsTests
//
//  Created by Maxime Britto on 17/07/2018.
//  Copyright © 2018 Purple Giraffe. All rights reserved.
//

import XCTest
import RealmSwift
@testable import LifeGoals

class GoalTests: XCTestCase {
    var _goalManager:GoalManager!
    
    override func setUp() {
        super.setUp()
        _goalManager = GoalManager(realmConfig: Realm.Configuration(inMemoryIdentifier: self.name))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIsDone() {
        let goalIndex = _goalManager.addGoal(withText: "Objectif 1")!
        let goal = _goalManager.getGoal(atIndex: goalIndex)
        XCTAssertEqual(false, goal.isDone)
        goal.addAchievement()
        XCTAssertEqual(true, goal.isDone)
        try! goal.realm!.write {
            goal.repeatDayFrequency = 1
        }
        XCTAssertEqual(true, goal.isDone)
        try! goal.realm!.write {
            goal.achievements.last!.date = Date(timeIntervalSinceNow: -2*24*60*60)
        }
        XCTAssertEqual(false, goal.isDone)
        try! goal.realm!.write {
            goal.repeatDayFrequency = 3
        }
        XCTAssertEqual(true, goal.isDone)
        goal.addAchievement()
        XCTAssertEqual(true, goal.isDone)
        try! goal.realm!.write {
            goal.repeatDayFrequency = 1
            goal.achievements.last!.date = Date(timeIntervalSinceNow: -2*24*60*60)
        }
        XCTAssertEqual(false, goal.isDone)
        goal.addAchievement()
        XCTAssertEqual(true, goal.isDone)
    }
    
    func testAddAchievements() {
        
    }
 
    
}
