//
//  NotificationViewController.swift
//  LifeGoalsNotifications
//
//  Created by Maxime Britto on 19/07/2018.
//  Copyright © 2018 Purple Giraffe. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var label: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preferredContentSize = CGSize(width: 320, height: 100)
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if let goalTitle = self.label?.text,
            let goal = GoalManager().getGoal(withTitle: goalTitle),
            sender.isOn == true {
            goal.addAchievement()
        }
    }
    func didReceive(_ notification: UNNotification) {
        self.label?.text = notification.request.content.title
    }

}
