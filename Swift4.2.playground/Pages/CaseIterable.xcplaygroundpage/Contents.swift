enum Family : CaseIterable {
    case Stark
    case Targaryen
    case Lannister
}

for name in Family.allCases {
    print(name)
}

