//: [Previous](@previous)

struct House : Equatable {
    let name:String
    let region:String
}

let h1 = House(name: "Stark", region: "Winterfell")
let h2 = House(name: "Lannister", region: "Casterly rock")

if h1 == h2 {
    print("Same house")
}

let group1:[House] = [h1,h2]
let group2:[House] = [h1,h2]

if group1 == group2 {
    print("Same groups")
}

//: [Next](@next)
